package nl.jusx.pycharm.lineprofiler.service;

public enum TimeFractionCalculation {
    FUNCTION_TOTAL,
    FUNCTION_MAX_LINE_TIME
}
